<?php

/**
 * Class GameofLife
 */
class GameofLife extends \CI_Model
{
	/**
	 * GameofLife constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('grid');
		$this->load->library('cell', $this->grid->getGrid());
	}

	/**
	 * With the provided data this will build next generation
	 * @param null $data
	 * @return array
	 */
	public function getNextGeneration($data = null): array
	{
		if (isset($data['gridWidth']) && $data['gridHeight']) {
			$this->grid->setGridWidth($data['gridWidth']);
			$this->grid->setGridHeight($data['gridHeight']);
			$this->grid->initGrid();
		}

		if (isset($data['curGrid']) && is_array($data['curGrid'])) {
			$this->grid->fillGrid($data['curGrid']);
		} else {
			$this->grid->fillGrid();
		}
		$this->cell->updateGrid($this->grid->getGrid());

		for ($row = 1; $row <= $this->grid->getGridWidth(); ++$row) {
			for ($col = 1; $col <= $this->grid->getGridHeight(); ++$col) {
				$availableLives = $this->cell->getLivingNeighborsCount($row, $col);
				$curPosValue = $this->grid->getGridValueAt($row, $col);
				$this->grid->setNextGridValueAt($row, $col, $curPosValue);
				switch ($curPosValue) {
					case 0:
						// Rule 4
						if ($availableLives == 3) {
							$this->grid->setNextGridValueAt($row, $col, 1);
						}
						break;
					case 1:
						// Rule 1 and 3
						if (($availableLives < 2) || ($availableLives > 3)) {
							$this->grid->setNextGridValueAt($row, $col, 0);
						}
				}
			}
		}
		return $this->grid->getNextGrid();
	}


}
