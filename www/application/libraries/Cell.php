<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Cell
 */
class Cell
{
	/**
	 * @var $grid
	 */
	private $grid;

	/**
	 * Cell constructor.
	 * @param array $grid
	 */
	public function __construct(array $grid)
	{
		$this->grid = $grid;
	}

	/**
	 * @param array $grid
	 */
	public function updateGrid(array $grid)
	{
		$this->grid = $grid;
	}

	/**
	 * Returns the neighbour count
	 * @param $row
	 * @param $col
	 * @return int
	 */
	public function getLivingNeighborsCount($row, $col): int
	{
		$count = 0;
		// iterate neighbor cells
		for ($x = -1; $x <= 1; $x++) {
			for ($y = -1; $y <= 1; $y++) {
				$count += $this->checkNeighborCellAliveAt($row + $x, $col + $y);
			}
		}
		// subtract own cell value
		$count -= $this->checkNeighborCellAliveAt($row, $col);

		return $count;
	}

	/**
	 * Check whether neighbour alive or not
	 * @param $row
	 * @param $col
	 * @return int
	 */
	private function checkNeighborCellAliveAt($row, $col): int
	{
		return isset($this->grid[$row][$col]) ? $this->grid[$row][$col] : 0;
	}
}
