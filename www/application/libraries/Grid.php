<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Grid
 */
class Grid
{
	const DEFAULT_GRID_SIZE = 38;
	/**
	 * @var $grid
	 */
	protected $grid;

	/**
	 * @var $nextGrid
	 */
	protected $nextGrid;

	/**
	 * @var $gridWidth
	 */
	protected $gridWidth;

	/**
	 * @var $gridHeight
	 */
	protected $gridHeight;


	/**
	 * Grid constructor.
	 */
	public function __construct()
	{
		$this->initGrid();
	}

	/**
	 * Initialize grid
	 */
	public function initGrid()
	{
		$emptyGrid = array_fill(1, $this->gridWidth ?: self::DEFAULT_GRID_SIZE, array_fill(1, $this->gridHeight ?: self::DEFAULT_GRID_SIZE, 0));
		$this->grid = $emptyGrid;
		$this->nextGrid = $emptyGrid;
	}

	/**
	 * Fill the current grid with provided values or random values
	 * @param null $gridCells
	 */
	public function fillGrid($gridCells = null)
	{
		for ($row = 1; $row <= $this->gridWidth; ++$row) {
			for ($col = 1; $col <= $this->gridHeight; ++$col) {
				$this->grid[$row][$col] = ($gridCells && isset($gridCells[$row][$col])) ? $gridCells[$row][$col] : mt_rand(0, 1);
			}
		}
	}

	/**
	 * Returns current grid
	 * @return array
	 */
	public function getGrid(): array
	{
		return $this->grid;
	}

	/**
	 * Returns next generation grid
	 * @return mixed
	 */
	public function getNextGrid(): array
	{
		return $this->nextGrid;
	}

	/**
	 * Set grid width
	 * @param int $gridWidth
	 */
	public function setGridWidth(int $gridWidth)
	{
		$this->gridWidth = $gridWidth;
	}

	/**
	 * Returns grid width
	 * @return int
	 */
	public function getGridWidth()
	{
		return $this->gridWidth;
	}

	/**
	 * Set grid height
	 * @param int $gridHeight
	 */
	public function setGridHeight(int $gridHeight)
	{
		$this->gridHeight = $gridHeight;
	}

	/**
	 * Returns grid height
	 * @return int
	 */
	public function getGridHeight()
	{
		return $this->gridHeight;
	}

	/**
	 * Set the value in next generation grid
	 * @param int $x
	 * @param int $y
	 * @param int $value
	 */
	public function setNextGridValueAt(int $x, int $y, int $value)
	{
		$this->nextGrid[$x][$y] = $value;
	}

	/**
	 * Returns grid value at
	 * @param int $x
	 * @param int $y
	 * @return int
	 */
	public function getGridValueAt(int $x, int $y): int
	{
		return $this->grid[$x][$y];
	}
}
