<?php
/**
 * Part of ci-phpunit-test
 *
 * @author     Kenji Suzuki <https://github.com/kenjis>
 * @license    MIT License
 * @copyright  2015 Kenji Suzuki
 * @link       https://github.com/kenjis/ci-phpunit-test
 */

class Game_test extends TestCase
{
	public function test_index()
	{
		$output = $this->request('GET', 'game/index');
		$this->assertContains('<title>Game of Life</title>', $output);
	}

	public function test_method_404()
	{
		$this->request('GET', 'game/method_not_exist');
		$this->assertResponseCode(404);
	}

	public function test_method_getNextGenJson_request(){
		$this->request('GET', 'game/getNextGenJson');
		$this->assertResponseCode(200);
	}

//	public function test_method_getNextGenJson_response(){
//		$inputJsonStr = '{"1":{"1":0,"2":1,"3":0,"4":0,"5":0,"6":0,"7":0,"8":0,"9":1,"10":1},"2":{"1":0,"2":1,"3":0,"4":1,"5":0,"6":1,"7":0,"8":1,"9":0,"10":0},"3":{"1":0,"2":1,"3":0,"4":0,"5":1,"6":1,"7":0,"8":0,"9":0,"10":0},"4":{"1":0,"2":1,"3":1,"4":1,"5":1,"6":1,"7":1,"8":1,"9":1,"10":0},"5":{"1":1,"2":0,"3":0,"4":0,"5":1,"6":0,"7":0,"8":0,"9":0,"10":1},"6":{"1":1,"2":1,"3":0,"4":1,"5":0,"6":1,"7":0,"8":1,"9":0,"10":0},"7":{"1":0,"2":1,"3":0,"4":1,"5":0,"6":0,"7":0,"8":0,"9":0,"10":0},"8":{"1":1,"2":1,"3":0,"4":1,"5":0,"6":1,"7":0,"8":1,"9":0,"10":0},"9":{"1":1,"2":0,"3":0,"4":0,"5":1,"6":1,"7":1,"8":0,"9":0,"10":0},"10":{"1":0,"2":0,"3":1,"4":1,"5":1,"6":1,"7":0,"8":0,"9":1,"10":0}}';
//		$expectJsonSt = '{"1":{"1":0,"2":0,"3":1,"4":0,"5":0,"6":0,"7":0,"8":0,"9":1,"10":0},"2":{"1":1,"2":1,"3":0,"4":0,"5":0,"6":1,"7":1,"8":0,"9":1,"10":0},"3":{"1":1,"2":1,"3":0,"4":0,"5":0,"6":0,"7":0,"8":0,"9":1,"10":0},"4":{"1":1,"2":1,"3":1,"4":0,"5":0,"6":0,"7":1,"8":1,"9":1,"10":0},"5":{"1":1,"2":0,"3":0,"4":0,"5":0,"6":0,"7":0,"8":0,"9":0,"10":0},"6":{"1":1,"2":1,"3":0,"4":1,"5":0,"6":0,"7":0,"8":0,"9":0,"10":0},"7":{"1":0,"2":0,"3":0,"4":1,"5":0,"6":0,"7":0,"8":0,"9":0,"10":0},"8":{"1":1,"2":1,"3":0,"4":1,"5":0,"6":1,"7":0,"8":0,"9":0,"10":0},"9":{"1":1,"2":0,"3":0,"4":0,"5":0,"6":0,"7":0,"8":1,"9":0,"10":0},"10":{"1":0,"2":0,"3":0,"4":1,"5":0,"6":0,"7":1,"8":0,"9":0,"10":0}}';
//		$data = [
//			"gridWidth"=> 10,
//			"gridHeight"=>10,
//			"curGrid"=> json_decode($inputJsonStr, true)
//		];
//		$this->request->setHeader('Accept', 'application/json');
//		$response = $this->ajaxRequest('POST', ['Game', 'getNextGenJson'],$data);
//		$this->assertJsonStringEqualsJsonString(
//			$expectJsonSt,
//			$response
//		);
//		$this->assertResponseCode(200);
//		$this->assertResponseHeader(
//			'Content-Type', 'application/json;'
//		);
//	}

}
