<?php
/**
 * Part of ci-phpunit-test
 *
 * @author     Kenji Suzuki <https://github.com/kenjis>
 * @license    MIT License
 * @copyright  2015 Kenji Suzuki
 * @link       https://github.com/kenjis/ci-phpunit-test
 */

class Grid_test extends TestCase
{
	public $object;

	public function setUp()
	{
		$this->object = $this->newLibrary('Grid');
	}
	public function test_initGrid_blank()
	{
		$this->object->initGrid();
		$this->assertEquals(array_fill(1, 38, array_fill(1, 38, 0)), $this->object->getGrid());
	}

	public function test_initGrid_custom_size()
	{
		$size = rand(5,30);
		$this->object->setGridWidth($size);
		$this->object->setGridHeight($size);
		$this->object->initGrid();
		$this->assertEquals(array_fill(1, $size, array_fill(1, $size, 0)), $this->object->getGrid());
	}

	public function test_fillGrid(){
		$size = rand(5,30);
		$matrix2D = [];
		for ($x = 1; $x <= $size; $x++)
		{
			for ($y = 1; $y <= $size; $y++)
			{
				$matrix2D[$x][$y]=rand(0,1);
			}
		}
		$this->object->setGridWidth($size);
		$this->object->setGridHeight($size);
		$this->object->initGrid();
		$this->object->fillGrid($matrix2D);
		$this->assertEquals($matrix2D,$this->object->getGrid());
	}
}
