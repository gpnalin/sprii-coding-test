<?php
/**
 * Part of ci-phpunit-test
 *
 * @author     Kenji Suzuki <https://github.com/kenjis>
 * @license    MIT License
 * @copyright  2015 Kenji Suzuki
 * @link       https://github.com/kenjis/ci-phpunit-test
 */

class Cell_test extends TestCase
{
	public $gridMatrix = [
		1 =>
			[
				1 => 0,
				2 => 1,
				3 => 0,
				4 => 0,
				5 => 0,
				6 => 0,
				7 => 0,
				8 => 0,
				9 => 1,
				10 => 1,
			],
		2 =>
			[
				1 => 0,
				2 => 1,
				3 => 0,
				4 => 1,
				5 => 0,
				6 => 1,
				7 => 0,
				8 => 1,
				9 => 0,
				10 => 0,
			],
		3 =>
			[
				1 => 0,
				2 => 1,
				3 => 0,
				4 => 0,
				5 => 1,
				6 => 1,
				7 => 0,
				8 => 0,
				9 => 0,
				10 => 0,
			],
		4 =>
			[
				1 => 0,
				2 => 1,
				3 => 1,
				4 => 1,
				5 => 1,
				6 => 1,
				7 => 1,
				8 => 1,
				9 => 1,
				10 => 0,
			],
		5 =>
			[
				1 => 1,
				2 => 0,
				3 => 0,
				4 => 0,
				5 => 1,
				6 => 0,
				7 => 0,
				8 => 0,
				9 => 0,
				10 => 1,
			],
		6 =>
			[
				1 => 1,
				2 => 1,
				3 => 0,
				4 => 1,
				5 => 0,
				6 => 1,
				7 => 0,
				8 => 1,
				9 => 0,
				10 => 0,
			],
		7 =>
			[
				1 => 0,
				2 => 1,
				3 => 0,
				4 => 1,
				5 => 0,
				6 => 0,
				7 => 0,
				8 => 0,
				9 => 0,
				10 => 0,
			],
		8 =>
			[
				1 => 1,
				2 => 1,
				3 => 0,
				4 => 1,
				5 => 0,
				6 => 1,
				7 => 0,
				8 => 1,
				9 => 0,
				10 => 0,
			],
		9 =>
			[
				1 => 1,
				2 => 0,
				3 => 0,
				4 => 0,
				5 => 1,
				6 => 1,
				7 => 1,
				8 => 0,
				9 => 0,
				10 => 0,
			],
		10 =>
			[
				1 => 0,
				2 => 0,
				3 => 1,
				4 => 1,
				5 => 1,
				6 => 1,
				7 => 0,
				8 => 0,
				9 => 1,
				10 => 0,
			],
	];
	public $object;

	public function setUp()
	{
		$this->object = new Cell($this->gridMatrix);
	}
	public function test_getLivingNeighborsCount()
	{
		$this->assertEquals( 4, $this->object->getLivingNeighborsCount(2,5));
	}

}
