<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Game of Life</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<style type="text/css">
		::selection {
			background-color: #E13300;
			color: white;
		}

		::-moz-selection {
			background-color: #E13300;
			color: white;
		}

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}

		a {
			color: #003399;
			background-color: transparent;
			font-weight: normal;
		}

		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 19px;
			font-weight: normal;
			margin: 0 0 14px 0;
			padding: 14px 15px 10px 15px;
		}

		code {
			font-family: Consolas, Monaco, Courier New, Courier, monospace;
			font-size: 12px;
			background-color: #f9f9f9;
			border: 1px solid #D0D0D0;
			color: #002166;
			display: block;
			margin: 14px 0 14px 0;
			padding: 12px 10px 12px 10px;
		}

		#body {
			margin: 0 15px 0 15px;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}

		#button-set {
			margin: 10px 0;
		}

		canvas {
			margin-bottom: 15px;
		}

		form {
			display: inline-block;
			vertical-align: top;
			margin-left: 10px;
		}
	</style>
</head>
<body>

<div id="container">
	<h1>Game of Life!</h1>
	<div id="body">
		<canvas id="grid"></canvas>
		<form action="javascript:void(0);" id="gameConfig">
			<div id="pattern-types">
				<b>Patterns: </b>
				<span><input type="radio" name="gameType" value="random" checked="checked"> Random</span>
				<span><input type="radio" name="gameType" value="ggg"> Gosper Glider Gun</span>
				<span><input type="radio" name="gameType" value="rpentomino">R-Pentomino</span>
				<span><input type="radio" name="gameType" value="glider">Glider</span>
				<span><input type="radio" name="gameType" value="grower">Grower</span>
				<span><input type="radio" name="gameType" value="horizontal">Horizontal</span>
				<span><input type="radio" name="gameType" value="diehard">Diehard</span>
				<span><input type="radio" name="gameType" value="acorn">Acorn</span>
			</div>
			<div id="button-set">
				<button class="controller-buttons" id="generate">Generate Pattern</button>
				<button class="controller-buttons" id="start" disabled>Start</button>
				<button class="controller-buttons" id="nextStep" disabled="disabled">Next Step</button>
				<button class="controller-buttons" id="stop" disabled="disabled">Stop</button>
				<button class="controller-buttons" id="clear" disabled="disabled">Clear</button>
			</div>
		</form>
	</div>

	<script !src="">
		var inProgress = false;
		var canvas = document.getElementById("grid");
		var cellWidth = 15;
		var cellHeight = 15;
		var gridWidth = 38;
		var gridHeight = 38;
		var cellColour = new Array("#fff", "#666");
		var curGrid = {};
		var refreshInterval;
		var Patterns = {
			'rpentomino': [[25, 25], [25, 26], [25, 27], [24, 26], [26, 25]],
			'glider': [[2, 4], [3, 4], [4, 4], [4, 3], [3, 2]],
			'ggg': [[3, 7], [4, 7], [3, 8], [4, 8],
				[13, 7], [13, 8], [13, 9], [14, 6], [14, 10], [15, 5], [16, 5], [15, 11], [16, 11], [17, 8], [18, 6], [18, 10], [19, 7], [19, 8], [19, 9], [20, 8],
				[23, 5], [24, 5], [23, 6], [24, 6], [23, 7], [24, 7], [25, 4], [25, 8], [27, 3], [27, 4], [27, 8], [27, 9],
				[37, 5], [37, 6], [38, 5], [38, 6]],
			'grower': [[20, 30], [22, 30], [22, 29], [24, 28], [24, 27], [24, 26], [26, 27], [26, 26], [26, 25], [27, 26]],
			'horizontal': [[5, 25], [6, 25], [7, 25], [8, 25], [9, 25], [10, 25], [11, 25], [12, 25],
				[14, 25], [15, 25], [16, 25], [17, 25], [18, 25],
				[22, 25], [23, 25], [24, 25],
				[31, 25], [32, 25], [33, 25], [34, 25], [35, 25], [36, 25],
				[38, 25], [39, 25], [40, 25], [41, 25], [42, 25]],
			'diehard': [[19, 25], [20, 25], [20, 26], [24, 26], [25, 26], [26, 26], [25, 24]],
			'acorn': [[20, 26], [21, 26], [21, 24], [23, 25], [24, 26], [25, 26], [26, 26]]
		};

		function drawMatrix() {
			if (canvas.getContext) {
				canvas.width = (cellWidth * gridWidth) + 1;
				canvas.height = (cellHeight * gridHeight) + 1;
				var ctx = canvas.getContext("2d");
				for (var row = 1; row <= gridHeight; ++row) {
					curGrid[row] = {};
					for (var col = 1; col <= gridWidth; ++col) {
						curGrid[row][col] = 0;
						drawCellLine(ctx, row, col);
					}
				}
			}
		}

		function drawGrid(isRandom =false) {
			for (var row = 1; row <= gridHeight; ++row) {
				for (var col = 1; col <= gridWidth; ++col) {
					if (isRandom) {
						curGrid[row][col] = Math.floor(Math.random() * Math.floor(2));
					} else {
						curGrid[row][col] = 0;
					}
				}
			}
		}

		function populateGrid(Patern) {
			Patern.forEach(function (coordinates) {
				var x = coordinates[0] - 1, y = coordinates[1] - 1;
				if (curGrid[x] === undefined || curGrid[x][y] === undefined) {
					return curGrid;
				}
				curGrid[x][y] = 1;
			});
			return curGrid;
		}

		function drawCellLine(ctx, row, col) {
			var x = (row - 1) * cellWidth + 0.5;
			var y = (col - 1) * cellHeight + 0.5;
			ctx.strokeStyle = "#8cdbff";
			ctx.strokeRect(x, y, cellWidth, cellHeight);
		}

		function fillCell(ctx, row, col, c) {
			var x = (row - 1) * cellWidth + 1;
			var y = (col - 1) * cellHeight + 1;
			ctx.fillStyle = c;
			ctx.fillRect(x, y, cellWidth - 1, cellHeight - 1);
		}

		function getNextGeneration() {

			if (!inProgress && checkGridState()) {
				inProgress = true;
				var postData = { "gridData":JSON.stringify({"gridWidth": gridWidth, "gridHeight": gridHeight, "curGrid": curGrid})};
				$.post('<?php echo base_url()."game/getNextGenJson"; ?>', postData, function (results) {
					curGrid = results;
					drawGeneration(curGrid);
					inProgress = false;
				}, "json");
			} else {
				$("#generate").attr("disabled", false);
				$("#start").attr("disabled", true);
				$("#nextStep").attr("disabled", true);
				$("#clear").attr("disabled", true);
			}
		}

		function checkGridState() {
			for (var row = 1; row <= gridHeight; ++row) {
				for (var col = 1; col <= gridWidth; ++col) {
					if (curGrid[row][col] === 1) {
						return true;
					}
				}
			}
			return false;
		}

		function drawGeneration(curGeneration) {
			if (canvas.getContext) {
				var ctx = canvas.getContext("2d");

				for (var row = 1; row <= gridHeight; ++row) {
					for (var col = 1; col <= gridWidth; ++col) {
						fillCell(ctx, row, col, cellColour[curGeneration[row][col]]);
					}
				}
			}

		}

		$(document).ready(function () {
			drawMatrix();
			$("#generate").click(function () {
				var radioVal = $('input[name=gameType]:checked', '#gameConfig').val();
				if (radioVal === 'random') {
					drawGrid(true);
				} else {
					populateGrid(Patterns[radioVal]);
				}
				drawGeneration(curGrid);
				$(this).attr("disabled", true);
				$("#start").attr("disabled", false);
				$("#nextStep").attr("disabled", false);
			});
			$("#start").click(function () {
				refreshInterval = setInterval(function () {
					getNextGeneration();
				}, 400);
				$(this).attr("disabled", true);
				$("#nextStep").attr("disabled", true);
				$("#stop").attr("disabled", false);
				$("#clear").attr("disabled", true);
			});

			$("#nextStep").click(function () {
				getNextGeneration();
				$("#clear").attr("disabled", false);
			});
			$("#stop").click(function () {
				clearInterval(refreshInterval);
				$("#clear").attr("disabled", false);
				$("#start").attr("disabled", false);
				$("#nextStep").attr("disabled", false);
				$(this).attr("disabled", true);
			});

			$("#clear").click(function () {
				drawGrid();
				drawGeneration(curGrid);
				$("#start").attr("disabled", true);
				$("#generate").attr("disabled", false);
				$("#nextStep").attr("disabled", true);
				$(this).attr("disabled", true);
			});
		});
	</script>
</div>

</body>
</html>
