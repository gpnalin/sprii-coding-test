<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Game
 */
class Game extends CI_Controller {

	/**
	 * Game constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('GameOfLife');
	}

	/**
	 * Index page
	 */
	public function index()
	{
		$this->load->view('game');

	}

	/**
	 * Ajax endpoin for get the next generation
	 */
	public function getNextGenJson(){
		header('Content-Type: application/json');
		$postData = $this->input->post("gridData");
		$nextGenGrid = $this->GameOfLife->getNextGeneration(json_decode($postData,true));

		echo json_encode($nextGenGrid);
	}
}
