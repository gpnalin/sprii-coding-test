<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Welcome
 */
class Welcome extends CI_Controller {

	/**
	 * Homepage
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
}
