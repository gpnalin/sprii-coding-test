# Game Of Life

Game of life app developed for check my knowledge & competency.

### Features
  - Can generate generations (if the grid empty there will be no requests to server)
  - Able to iterate step by step generations
  - Can start with multiple patterns such as,
    - Gosper Glider Gun
    - R-Pentomino
    - Glider
    - Grower
    - Horizontal
    - Diehard
    - Acorn
    - or Random pattern
  - Application developed to support any grid size
  - Change the pattern or mode whenever you like. no need to reload the app.

### Installation

Please follow below step to run the application.

```sh
# Extract the archive.
$ tar -xvzf sprii.tar.gz

# Navigate to sprii directory
$ cd sprii

# Use docker-compose to spin up the server
$ docker-compose up
```

### Usage Guide

Visit **[10.0.0.1:8080]** to check the application. Application itself is self-explanatory.

### Testing
We declared a **cli** service on *docker-compose.yml* using php-cli image to run tests. So you are able to run tests simple as below.
```sh
$ docker-compose exec cli php vendor/bin/phpunit -c application/tests 
```

### Tech

Game of life utilizes a number of open source projects:

  - [Codeigniter] - Simple PHP Framework
  - [Composer] - PHP Dependency manager
  - [jQuery] - JS Library
  - [PHPUnit] - PHP Unitest library

   [Codeigniter]: <https://www.codeigniter.com>
   [Composer]: <https://getcomposer.org/>
   [jQuery]: <http://jquery.com>
   [10.0.0.1:8080]: <http://10.0.0.1:8080/>
   [PHPUnit]: <https://phpunit.de/>